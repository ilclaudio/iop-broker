##############################################################################
###### 				USEFUL DOCKER COMMANDS:								
######																	
######	- docker build -t mosquitto_broker -f Dockerfile .					
######  - docker run --name iop-broker -d  -p 1883:1883  mosquitto_broker 
######	- docker exec -it iop-broker /bin/bash							 					
######																	
##############################################################################

###### Start from the official Eclipse Mosquito image based on Alpine ######
FROM eclipse-mosquitto:latest

### Update Alpine distribution and install useful tools ###
RUN apk update
RUN apk upgrade
RUN apk add bash
RUN apk add bash-completion

# Define volume mount points
VOLUME ["/mosquitto/data", "/mosquitto/log", "/mosquitto/conf"]

# Add the link to the custom configurations directory
RUN echo "include_dir /mosquitto/config/custom" > /mosquitto/config/mosquitto.conf
COPY ./config/custom /mosquitto/config/custom

# Add the file with the allowed accounts
COPY ./config/auth /mosquitto/config/auth
# Encrypt passwords
RUN mosquitto_passwd -U /mosquitto/config/auth/pwd.txt

EXPOSE 1883